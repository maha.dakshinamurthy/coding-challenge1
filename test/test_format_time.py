def test_one_second():
    assert format_time(1) == "1 second"

def test_zero_seconds():
    assert format_time(0) == "none"

def test_two_seconds():
    assert format_time(2) == "2 seconds"

def test_three_seconds():
    assert format_time(3) == "3 seconds"

def test_one_minute():
    assert format_time(60) == "1 minute"

def test_two_minutes():
    assert format_time(2*60) == "2 minutes"

def test_two_minutes_30_seconds():
    assert format_time(2*60 + 30) == "2 minutes and 30 seconds"

def test_one_hour():
    assert format_time(2*60 + 30) == "2 minutes and 30 seconds"
    
def format_time(seconds):
    if seconds == 0:
        return "none"
    
    if seconds == 1:
        return "1 second"

    if seconds < 60:
        return f"{seconds} seconds"

    minutes = seconds // 60
    remainder = seconds % 60
    if minutes == 1:
        return "1 minute"
    
    if remainder == 0:
        return f"{minutes} minutes"

    return f"{minutes} minutes and {remainder} seconds"



